package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck {
    public ModelDuck(){
        setFlyBehavior(new FlyNoWay());
        setQuackBehavior(new Quack());
    }

    public void display(){
        System.out.println("I'm a model duck");
    }
// TODO Complete me!
}
