package id.ac.ui.cs.advprog.tutorial1.strategy;

public interface FlyBehavior {

   public void fly();
}
